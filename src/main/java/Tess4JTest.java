
import net.sourceforge.tess4j.ITessAPI;
import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import net.sourceforge.tess4j.util.ImageHelper;
import net.sourceforge.tess4j.util.LoadLibs;
import org.apache.commons.io.IOUtils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Tess4J测试类
 */
public class Tess4JTest {

    public static void main(String[] args) {
//        String path = System.getProperty("user.dir") +"\\src\\main\\resources"; //src\main\resources
//        File imageFile = new File(path + "/imgcode.png");
//        Tesseract tessInst = new Tesseract();
//        System.out.println( path + "\\tessdata" );
//        tessInst.setDatapath(path + "\\tessdata");
//        tessInst.setTessVariable("user_defined_dpi", "300");
//        tessInst.setLanguage("eng");// eng.traineddata is in /tessdata direcotry
//        System.out.println( imageFile );
//        Rectangle rectangle = new Rectangle(0, 0, 763, 534);// recognize special area letters
//        try {
//
//
//            String result= tessInst.doOCR(imageFile, rectangle);
//            System.out.println("result:"+result);
//        } catch (TesseractException e) {
//            System.err.println(e.getMessage());
//        }


        String path = "C:\\Users\\HXYD\\Desktop";        //我的项目存放路径

        File file = new File(path + "\\imgcode.png");
        ITesseract instance = new Tesseract();

        /**
         *  获取项目根路径，例如： D:\IDEAWorkSpace\tess4J
         */
        File directory = new File("C:\\idealproject\\tess4J\\src\\main\\resources");
        String courseFile = null;
        try {
            courseFile = directory.getCanonicalPath();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //设置训练库的位置
        instance.setDatapath(courseFile + "/tessdata");
        instance.setTessVariable("user_defined_dpi", "300");
        instance.setLanguage("eng");//chi_sim ：简体中文， eng	根据需求选择语言库
        instance.setOcrEngineMode(ITessAPI.TessOcrEngineMode.OEM_TESSERACT_ONLY);
        instance.setPageSegMode(ITessAPI.TessPageSegMode.PSM_SINGLE_LINE);


        instance.setTessVariable("tessedit_char_whitelist ","ABCDEFGHIJKLMNOPQRSTUVWXYZ");
//        instance.setTessVariable("tessedit_char_blacklist ","0123456789,‘.");



        String result = null;
        try {
            long startTime = System.currentTimeMillis();

            String url = "http://jw.xujc.com/imgcode.php?0.8243840224757777";
            BufferedImage bin = ImageIO.read(new URL(url));
            bin = process(bin);
            result = instance.doOCR(bin);
            long endTime = System.currentTimeMillis();
            System.out.println("Time is：" + (endTime - startTime) + " 毫秒");
        } catch (TesseractException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("result: ");
        System.out.println(result);
    }


    public static BufferedImage process(BufferedImage bin) {

        int endX = bin.getWidth();
        int endY = bin.getHeight();
        // 这里对图片黑白处理,增强识别率.这里先通过截图,截取图片中需要识别的部分
//        BufferedImage textImage = ImageHelper.getSubImage(bin, 0, 0, endX, endY);

        BufferedImage textImage = ImageHelper.convertImageToGrayscale(bin);
//        textImage = snnFiltering( textImage);
        // 图片锐化,自己使用中影响识别率的主要因素是针式打印机字迹不连贯,所以锐化反而降低识别率
//        textImage = ImageHelper.convertImageToBinary(textImage);
        // 图片放大5倍,增强识别率(很多图片本身无法识别,放大5倍时就可以轻易识,但是考滤到客户电脑配置低,针式打印机打印不连贯的问题,这里就放大5倍)
        textImage = ImageHelper.getScaledInstance(textImage, endX * 2, endY * 2);
//        textImage = ImageHelper.invertImageColor(textImage);


        AsFile(textImage);
        return textImage;
    }

    public static void AsFile(BufferedImage textImage) {
//        File pngFile = new File("D:\\" + System.currentTimeMillis() + ".png");
        File pngFile = new File("D:\\cover.png");
        OutputStream out = null;
        try {
            out = new FileOutputStream(pngFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            ImageIO.write(textImage, "png", out);
        } catch (IOException e) {
            e.printStackTrace();
        }
        IOUtils.closeQuietly(out);
    }

    /**
     * 对称近邻均值滤波
     */
    public static BufferedImage snnFiltering( BufferedImage img) {
        int w = img.getWidth();
        int h = img.getHeight();
        int[] pix = new int[w*h];
        img.getRGB(0, 0, w, h, pix, 0, w);
        int newpix[] = snnFiltering(pix, w, h);
        img.setRGB(0, 0, w, h, newpix, 0, w);
        return img ;
    }
    /**
     * 对称近邻均值滤波
     * @param pix 像素矩阵数组
     * @param w 矩阵的宽
     * @param h 矩阵的高
     * @return 处理后的数组
     */
    public static int[] snnFiltering(int pix[], int w, int h) {
        int newpix[] = new int[w*h];
        int n = 9;
        int temp, i1,i2, sum;
        int[] temp1 = new int[n];
        int[] temp2 = new int[n/2];
        ColorModel cm = ColorModel.getRGBdefault();
        int r=0;
        for(int y=0; y<h; y++) {
            for(int x=0; x<w; x++) {
                if(x!=0 && x!=w-1 && y!=0 && y!=h-1) {
                    sum = 0;
                    temp1[0] = cm.getRed(pix[x-1+(y-1)*w]);
                    temp1[1] = cm.getRed(pix[x+(y-1)*w]);
                    temp1[2] = cm.getRed(pix[x+1+(y-1)*w]);
                    temp1[3] = cm.getRed(pix[x-1+(y)*w]);
                    temp1[4] = cm.getRed(pix[x+(y)*w]);
                    temp1[5] = cm.getRed(pix[x+1+(y)*w]);
                    temp1[6] = cm.getRed(pix[x-1+(y+1)*w]);
                    temp1[7] = cm.getRed(pix[x+(y+1)*w]);
                    temp1[8] = cm.getRed(pix[x+1+(y+1)*w]);
                    for(int k=0; k<n/2; k++) {
                        i1 = Math.abs(temp1[n/2] - temp1[k]);
                        i2 = Math.abs(temp1[n/2] - temp1[n-k-1]);
                        temp2[k] = i1<i2 ? temp1[k] : temp1[n-k-1];  //选择最接近原像素值的一个邻近像素
                        sum = sum + temp2[k];
                    }
                    r = sum/(n/2);
                    //System.out.println("pix:" + temp1[4] + "  r:" + r);
                    newpix[y*w+x] = 255<<24 | r<<16 | r<<8 |r;
                } else {
                    newpix[y*w+x] = pix[y*w+x];
                }
            }
        }
        return newpix;
    }
}